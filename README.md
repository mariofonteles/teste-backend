# Ioasys Backend Challenge

API construída para o teste de Backend da Ioasys.

# Features

- RESTful API Construída com expressJS
- Sequelize para requisições ao banco de dados(postgreSQL)
- Testes Unitários com Mocha & Chai 
- Autenticação por JWT 
- ESLINT
- Docker

# Como Testar

- Abra a pasta raiz do projeto no seu Terminal favorito.
- execute o comando `npm install`
- execute o comando `docker-compose up --build`
- Após realização dos testes e inicialização do servidor, a API estará disponível em `localhost:8000/api`
 * obs: Caso utilize Docker Toolbox, ou configure o Docker de maneira diferente, troque o localhost pelo endereço da máquina Docker.
- Uma collection do Postman está contida dentro do projeto para facilitar os testes.
- Caso algum problema ocorra, `docker-compose down` irá parar o container e poderá ser reinicializado em seguida.

**Obrigado!**
