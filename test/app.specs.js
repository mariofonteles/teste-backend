// process.env.NODE_ENV = "test";

//Require the dev-dependencies
let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../src/app");
let should = chai.should();

chai.use(chaiHttp);

// testing with a separate test database
// clear data

/*
 * Test the /GET route
 */
describe("/GET root", () => {
  it("it should GET the root of the API", done => {
    chai
      .request(server)
      .get("/")
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });
});

describe("/GET root api", () => {
  it("it should GET root api", done => {
    chai
      .request(server)
      .get("/api")
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });
});

describe("/POST users", () => {
  it("it should POST user", done => {
    chai
      .request(server)
      .post('/api/auth').send({"email": "admin@test.com", "password": "admin"})
      .end(function(err, res){
        chai.request(server)
        .post("/api/users").set('authorization', 'Bearer ' +res.body.token)
        .send({
          "name": "sampleUser3",
          "admin": false,
          "status": "active",
          "email": "test3@tester.com",
          "password": 'admin',
        })
        .end((err, res) => {
          console.log(res.body);
          res.should.have.status(201);
          done();
        });
      })
  });
});

describe("/PUT user/:userId", () => {
  it("it should UPDATE user", done => {
      chai.request(server)
      .post('/api/auth').send({"email": "admin@test.com", "password": "admin"})
      .end(function(err, res){
        let token = res.body.token;
        chai.request(server).get('/api/users')
        .end(function(err, res){
          console.log(res.body);
          chai.request(server)
            .put('/api/users/'+res.body[1].id).set('authorization', 'Bearer ' +token)
            .send({
              "name": "sampleUser newName",
              "admin": false,
              "status": "active",
              "email": "test2@tester.com",
              "password": '123456',
            })
            .end(function(error, response){
              response.should.have.status(200);
              response.should.be.json;
              response.body.should.be.a('object');
              response.body.should.have.property('name');
              response.body.should.have.property('admin');
              response.body.should.have.property('email');
              response.body.should.have.property('status');
              response.body.should.have.property('password');
              response.body.should.have.property('id');
              response.body.name.should.equal('sampleUser newName');
              response.body.status.should.equal('active');
              done();
          });
        });
      });
  });
});

describe("/GET users", () => {
  it("it should GET all users", done => {
    chai
      .request(server)
      .get("/api/users")
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });
});

describe("/GET users/:userId", () => {
  it("it should GET one user by id ", done => {
      chai.request(server)
      .get('/api/users')
      .end(function(err, res){
        console.log(res.body);
        chai.request(server)
          .get('/api/users/'+res.body[0].id)
          .end(function(error, response){
            response.should.have.status(200);
            response.should.be.json;
            response.body.should.be.a('object');
            response.body.should.have.property('name');
            response.body.should.have.property('admin');
            response.body.should.have.property('email');
            response.body.should.have.property('status');
            response.body.should.have.property('password');
            response.body.should.have.property('id');
            done();
        });
      });
  });
});

describe("/DELETE users", () => {
  it("it should DELETE the user", done => {
      chai.request(server)
      .get('/api/users')
      .end(function(err, res){
        console.log(res.body[1])
        chai.request(server)
          .delete('/api/users/'+res.body[1].id)
          .end(function(error, response){
            response.should.have.status(401);
            response.should.be.json;
            response.body.should.be.a('object');
            done();
        });
      });
  });
});

describe("/PUT ratings", () => {
  it("it should PUT rating", done => {
    chai.request(server)
      .post('/api/auth').send({"email": "admin@test.com", "password": "admin"})
      .end(function(err, res){
        chai.request(server)
        .put('/api/movies/1/rating').set('authorization', 'Bearer ' +res.body.token)
        .send({
          "stars": 4,
        })
        .end((err, res) => {
          console.log(res.body);
          res.should.have.status(200);
          done();
        });
      })
  });
})
