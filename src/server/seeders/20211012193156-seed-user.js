'use strict';
const bcrypt = require("bcrypt");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const hashedPassword1 = await bcrypt.hash(String('admin'), bcrypt.genSaltSync(8));
    const hashedPassword2 = await bcrypt.hash(String('newuser'), bcrypt.genSaltSync(8));
    const hashedPassword3 = await bcrypt.hash(String('new2user'), bcrypt.genSaltSync(8));

    return queryInterface.bulkInsert('users', [
      {
        name: "Admin",
        admin: true,
        status: "active",
        email: "admin@test.com",
        password: hashedPassword1,
        createdAt : new Date(),
        updatedAt : new Date(),
      },
      {
        name: "SampleUser",
        admin: false,
        status: "active",
        email: "sample@user.com",
        password: hashedPassword2,
        createdAt : new Date(),
        updatedAt : new Date(),
      },

      {
        name: "SampleUser2",
        admin: false,
        status: "active",
        email: "sample2@user.com",
        password: hashedPassword3,
        createdAt : new Date(),
        updatedAt : new Date(),
      },
      ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user', [{
    }])
  }
};
