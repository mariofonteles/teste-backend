'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('movies', [
      {
        name: "Watchmen",
        director: "Zack Snyder",
        status: "active",
        genre: "Action",
        actors: "Patrick Wilson, Malin Akerman, Jeffrey Dean Morgan",
        createdAt : new Date(),
        updatedAt : new Date(),
      },
      {
        name: "The Dark Knight",
        director: "Christopher Nolan",
        status: "active",
        genre: "Action",
        actors: "Christian Bale, Heath Ledger, Michael Caine",
        createdAt : new Date(),
        updatedAt : new Date(),
      },
      ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('movie', [{
    }])
  }
};
