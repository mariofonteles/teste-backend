'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('ratings', [
      {
        movieId: 1,
        userId: 2,
        stars: 4,
        createdAt : new Date(),
        updatedAt : new Date(),
      },
      {
        movieId: 1,
        userId: 2,
        stars: 3,
        createdAt : new Date(),
        updatedAt : new Date(),
      },
      {
        movieId: 2,
        userId: 2,
        stars: 3,
        createdAt : new Date(),
        updatedAt : new Date(),
      },
      ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('rating', [{
    }])
  }
};
