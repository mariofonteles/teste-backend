const Movie = require("../models").movie;
const User = require("../models").user;

module.exports = (sequelize, DataTypes) => {
    const rating = sequelize.define('rating', {
      movieId: {
        type: DataTypes.INTEGER,
        references: {model: Movie, key: 'id'}
      },
      userId: {
        type: DataTypes.INTEGER,
        references: {model: User, key: 'id'}
      },
      stars: {
          type: DataTypes.INTEGER
      },
    }, {});

    rating.associate = (models) => {
        rating.belongsTo(models.movie);
        rating.belongsTo(models.user);
    }

    return rating;
  };