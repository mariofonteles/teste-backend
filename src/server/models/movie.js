'use strict';

const Rating = require("./rating");

module.exports = (sequelize, DataTypes) => {
  const movie = sequelize.define('movie', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    director: {
      type: DataTypes.STRING
    },
    genre: {
        type: DataTypes.STRING
    },
    actors: {
        type: DataTypes.STRING
    },
    status:{
      type: { type: DataTypes.ENUM('active','blocked') },
      allowNull: false,
      validate: {
        isIn: {
          args: [['active','blocked']],
          msg: "Invalid movie status"
        }
      }
    },
  }, {});

  movie.associate = (models) => {
    movie.hasMany(models.rating);
  }

  return movie;
};