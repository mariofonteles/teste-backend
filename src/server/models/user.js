'use strict';

const bcrypt = require("bcrypt");
const Rating = require("./rating");

module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    admin: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    status:{
      type: { type: DataTypes.ENUM('active','blocked') },
      allowNull: false,
      validate: {
        isIn: {
          args: [['active','blocked']],
          msg: "Invalid user status"
        }
      }
    },
  });

  user.beforeCreate(async (user, options) => {
    const hashedPassword = await bcrypt.hash(String(user.dataValues.password), bcrypt.genSaltSync(8));
    user.dataValues.password = hashedPassword;
  });

  user.beforeUpdate(async (user, options) => {
    if(user._changed.password == true){
      console.log('password changed');
      const hashedPassword = await bcrypt.hash(String(user.dataValues.password), bcrypt.genSaltSync(8));
      user.dataValues.password = hashedPassword;
    }
  });

  user.associate = (models) => {
    user.hasMany(models.rating);
  }
  
  return user;
};