const users = require("./users");
const movies = require("./movies");
const ratings = require("./ratings");

module.exports = {
  users,
  movies,
  ratings
};
