var Sequelize = require('sequelize');
const rating = require("../models").rating;


module.exports = {

    async addOrUpdate(req, res, next) {

        let existing = await rating.findOne({movieId: req.params.id, userId: req.userId});

        if(existing) {
            return existing.update(
                {stars: Number(req.body.stars), 
                returning: true})
            .then((entity) => {
                return res.status(200).send({entity, wasUpdated: true})
            })
            .catch((error) => {
                return res.status(400).send(error);
            })
        }

        rating.create(
            {movieId: req.params.id,
            userId: req.userId,
            stars: Number(req.body.stars)}, 
            {returning: true})
        .then( (entity) => {
            return res.status(200).send({entity, wasUpdated: false})
        })
        .catch( (error) => {
            return res.status(400).send(error);
        })
      },
};