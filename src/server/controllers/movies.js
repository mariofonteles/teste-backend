var Sequelize = require('sequelize');
const movie = require("../models").movie;
const rating = require("../models").rating;
const user = require("../models").user;

module.exports = {
  create(req, res, next) {
    if(req.isAdmin !== true) 
      return res.status(403).send({message: 'You do net have Admin access.'});

    return movie
      .create({
        name: req.body.name,
        director: req.body.director,
        genre: req.body.genre,
        actors: req.body.actors,
        status: req.body.status? req.body.status : 'active'})
      .then(created => {
        res.status(201).send(created);
      })
      .catch(error => res.status(400).send(error));
  },

  update(req, res, next) {
    if(req.isAdmin !== true) 
      return res.status(403).send({message: 'You do net have Admin access.'});

    return movie
      .update(req.body, {returning: true, where: {id: req.params.id} })
      .then(([ rowsUpdate, [updatedRow] ]) => res.status(200).send(updatedRow))
      .catch(error => res.status(400).send(error));
  },

  list(req, res) {
    //big query requires breaking it down
    let query = { attributes: ['id', 'name', 'director', 'actors', 'genre', 'status',
      [Sequelize.fn('AVG', Sequelize.col('ratings.stars')), 'avgRating'],
    ],
    include: [
      {
        model: rating,
        as: 'ratings',
        attributes: [],
      },
    ],
    group: ['movie.id']
    };

    console.log(req.params);
    //if a filter is applied, query for the filterable attributes
    if(req.params.filter)
      query.where = {
          [Sequelize.Op.or]: {
            genre: {[Sequelize.Op.iLike]: '%'+req.params.filter+'%'},
            name: {[Sequelize.Op.iLike]: '%'+req.params.filter+'%'},
            director: {[Sequelize.Op.iLike]: '%'+req.params.filter+'%'},
            actors: {[Sequelize.Op.iLike]: '%'+req.params.filter+'%'},
          }
      };

    return movie
      .findAll(query)
      .then(users => res.status(200).send(users))
      .catch(error => { console.log(error); return res.status(400).send(error)});
  },

  retrieve(req, res) {
    return movie
      .findByPk(req.params.id, { attributes: ['id', 'name', 'director', 'actors', 'genre', 'status',
          [Sequelize.fn('AVG', Sequelize.col('ratings.stars')), 'avgRating'],
        ],
        include: [
          {
            model: rating,
            as: 'ratings',
            attributes: [],
          },
        ],
        group: ['movie.id']
      })
      .then(entity => {
        console.log(entity);
        if (!entity) {
          throw({
              "name": "ValidationError",
              "errors": [
                  {
                    message:'Movie not found'
                  }
                ]
            });
        }
        return res.status(200).send(entity);
      })
      .catch(error => res.status(400).send(error));
  },

  delete(req, res, next) {
    if(req.isAdmin !== true) 
      return res.status(403).send({message: 'You do net have Admin access.'});

      return movie.update({status: "blocked"}, { returning: true, where: { id: req.params.id} })
      .then( () => 
        res.status(200).json({message:"Deleted successfully"}))
      .catch(error => res.status(400).send(error));
  }
};