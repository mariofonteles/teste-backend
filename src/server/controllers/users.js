var Sequelize = require('sequelize');
const jwt = require('jsonwebtoken');
const user = require("../models").user;
const bcrypt = require("bcrypt");

module.exports = {

  async auth(req, res) {
    try {
      entity = await user.findOne({ where: {email : req.body.email}});
      //Compares encrypted password to new password
      const isValid = await bcrypt.compare(req.body.password, entity.password);
      if (isValid) {
        let id = entity.id;
        const token = jwt.sign( { id: entity.id, admin: entity.admin }, process.env.SECRET, {
          expiresIn: 2000 //expires in 33 minutes
        });
        return res.status(200).send({auth: true, token: token, expires_in: 2000, token_type: "Bearer"});
      }
      else
        throw new Error("Incorrect Password!");
    }
    catch(error) {
      console.log(error);
      return res.status(403).send(error);
    }
  },

  //Middleware
  verifyJWT(req, res, next) {
    const token = req.headers['authorization'];
    if (!token)
      return res.status(401).send({auth: false, message: "Missing Auth Token!"});
    try{
    //expects header format to be authorization: bearer xxxxxxxxx
    let verified = jwt.verify(token.split('Bearer ')[1], process.env.SECRET);
    console.log(verified);
    req.userId = verified.id;
    req.isAdmin = verified.admin;
    next();
    }
    catch(err){
      return res.status(500).send({auth: false, message: "Failed Token Verification!", error: err})
    }
  },
  
  create(req, res, next) {
    if(req.isAdmin !== true) 
      return res.status(403).send({message: 'You do net have Admin access.'});
    
    return user
      .create({
        name: req.body.name,
        admin: req.body.admin,
        status: req.body.status,
        email: req.body.email,
        password: req.body.password
      })
      .then(user => {
        return res.status(201).send(user)
      
      })
      .catch(error => {
        return res.status(400).send(error)});
  },

  update(req, res, next) {
    if(req.isAdmin !== true) 
      return res.status(403).send({message: 'You do net have Admin access.'});

    return user
      .update({
        name: req.body.name,
        admin: req.body.admin,
        status: req.body.status,
        email: req.body.email,
        password: req.body.password
      },
      { individualHooks: true, returning: true, where: {id: req.params.id} })
      .then(([ rowsUpdate, [updatedRow] ]) => res.status(200).send(updatedRow))
      .catch(error => { console.log(error); return res.status(400).send(error)});
  },

  list(req, res) {
    return user
      .findAll()
      .then(users => res.status(200).send(users))
      .catch(error => { console.log(error); return res.status(400).send(error)});
  },

  retrieve(req, res) {
    return user
      .findByPk(req.params.id, {
      })
      .then(entity => {
        if (!entity) {
          throw({
            "name": "ValidationError",
            "errors": [
                {
                  message:'user not found'
                }
              ]
          });
        }
        return res.status(200).send(entity);
      })
      .catch(error => res.status(400).send(error));
  },

  delete(req, res, next) {
    if(req.isAdmin !== true) 
      return res.status(403).send({message: 'You do net have Admin access.'});
    
    return user.update({status: "blocked"}, { returning: true, where: { id: req.params.id} })
    .then( () => 
      res.status(200).json({message:"Deleted successfully"}))
    .catch(error => 
      { console.log(error); return res.status(400).send(error)
    });
  }

};
