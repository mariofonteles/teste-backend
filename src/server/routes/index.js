const usersController = require("../controllers").users;
const moviesController = require("../controllers").movies;
const ratingsController = require("../controllers").ratings;

module.exports = app => {
  app.get("/api", (req, res) =>
    res.status(200).send({
      message: "Welcome to the Movies API!"
    })
  );

  app.post('/api/auth', usersController.auth);

  app.post("/api/users", usersController.verifyJWT, usersController.create);
  app.put("/api/users/:id",usersController.verifyJWT, usersController.update);
  app.get("/api/users", usersController.list);
  app.get("/api/users/:id", usersController.retrieve);
  app.delete("/api/users/:id", usersController.verifyJWT, usersController.delete);

  app.post("/api/movies",usersController.verifyJWT, moviesController.create);
  app.put("/api/movies/:id",usersController.verifyJWT, moviesController.update);
  app.get("/api/movies", moviesController.list);
  app.get("/api/movies/filter/:filter", moviesController.list);
  app.get("/api/movies/:id", moviesController.retrieve);
  app.delete("/api/movies/:id", usersController.verifyJWT, moviesController.delete);

  app.put("/api/movies/:id/rating",usersController.verifyJWT, ratingsController.addOrUpdate);

};
